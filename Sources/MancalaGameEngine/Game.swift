//
//  MancalaGameBoard.swift
//  Mancala
//
//  Created by Sanford, Michael on 6/20/15.
//  Copyright © 2016 Sanford, Michael. All rights reserved.
//

import Foundation
import NetworkingCore
import GamingCore
import MancalaCore
import Dispatch

enum GameBoardError: Error {
    case invalidPit
    case invalidPlayer
    case invalidGameState
    case invalidMoveAttempt(message: String)
}

extension Timer: GameTimerProtocol {
    public func fireIfNeeded() {}
}

public typealias PitID = Int
public typealias SeedID = Int
typealias SeedStack = Stack<SeedID>

public class Pit: Hashable {
    fileprivate weak var game: Game!
    public let identifier: PitID
    public let playerIdentity: PlayerIdentity
    public let isStore: Bool
    
    fileprivate init(identifier: PitID, playerIdentity: PlayerIdentity, isStore: Bool) {
        self.identifier = identifier
        self.playerIdentity = playerIdentity
        self.isStore = isStore
    }
    
    public var numberOfSeeds: Int {
        return game.currentGameboardState[self]
    }
    
    public var isEmpty: Bool {
        return game.currentGameboardState.isEmpty(pit: self)
    }
    
    public var hashValue: Int {
        return identifier
    }
    
    public var isValidMove: Bool {
        return game.isValidMove(pit: self)
    }
}

public enum MoveType {
    case preview, normal, capture, storePit, reset, undo, skip
}

public enum DirectionType {
    case clockwise, counterClockwise
}

public enum GameEvent {
    case move(GameMove)
    case skipTurn(GameSkipTurn)
}

public struct GameMove {
    public let transitions: [GameTransition]
    public let gameState: GameState
    
    public init(transitions: [GameTransition], gameState: GameState) {
        self.transitions = transitions
        self.gameState = gameState
    }
}

public struct GameSkipTurn {
    public let skippingPlayer: PlayerIdentity
    public let gameState: GameState
    
    public init(skippingPlayer: PlayerIdentity, gameState: GameState) {
        self.skippingPlayer = skippingPlayer
        self.gameState = gameState
    }
}

public class Game: CustomDebugStringConvertible {
    
    public let pitsPerSide: Int
    public private(set) var seedsPerPit: Int
    public let direction: DirectionType
    public var startingTurnLogic: StartingTurnLogicType
    public private(set) var isTutorialEnabled: Bool = false
    
    private typealias MoveInfo = (player: PlayerIdentity, snapshot: GameSnapshot, transitions: [GameTransition])
    
    let pits: [Pit]
    private let player1StorePit: Pit
    private let player2StorePit: Pit
    public let numberOfPits: Int
    private let player1MovePits: [Pit]
    private let player2MovePits: [Pit]
    private let oppositePitMap: [Pit: Pit]
    fileprivate var initialGameboardState: GameboardState
    private let tutorialGameboardState: GameboardState
    public private(set) var startingPlayer: PlayerIdentity
    
    private struct GameSnapshot {
        let gameboardState: GameboardState
        let gameState: GameState
    }
    
    private var history: Stack<MoveInfo>
    
    public var turnTimeLimit: TimeInterval?
    public var countdownTime: TimeInterval?
    public private(set) var player1: PlayerType
    public private(set) var player2: PlayerType
    public private(set) var gameOverOnWinner: Bool
    private var bot1: Bot?
    private var bot2: Bot?
    public var botDidMakeMoveHandler: ((PlayerIdentity, GameMove) -> Void)?
    public var playerDidTimeoutTurnHandler: ((PlayerIdentity, GameState) -> Void)?
    public var playerDidStartTurnCountdownHandler: ((PlayerIdentity, Int) -> Void)?
    private var botMoveTimer: GameTimerProtocol?
    private var turnTimer: GameTimerProtocol?
    private var countdownTimer: GameTimerProtocol?
    public var container: GameContainerProtocol?
    
    public var numberOfMoves: Int {
        return history.count
    }
    
    private var currentPlayer: PlayerIdentity? {
        return currentGameState.currentPlayer
    }
    
    public var currentGameState: GameState {
        guard let currentHistory = history.first else { return .readyToPlay(currentPlayer: startingPlayer) }
        return currentHistory.snapshot.gameState
    }
    
    fileprivate var currentGameboardState: GameboardState {
        guard let currentHistory = history.first else { return isTutorialEnabled ? tutorialGameboardState : initialGameboardState }
        return currentHistory.snapshot.gameboardState
    }
    
    public init(pitsPerSide: Int = 6,
                seedsPerPit: Int = 4,
                direction: DirectionType = .counterClockwise,
                startingTurnLogic: StartingTurnLogicType = .one,
                turnTimeLimit: TimeInterval? = nil,
                countdownTime: TimeInterval? = nil,
                gameOverOnWinner: Bool = false,
                player1: PlayerType = .human,
                player2: PlayerType) {
        
        self.pitsPerSide = pitsPerSide
        self.seedsPerPit = seedsPerPit
        self.direction = direction
        self.startingTurnLogic = startingTurnLogic
        self.turnTimeLimit = turnTimeLimit
        self.countdownTime = countdownTime
        self.gameOverOnWinner = gameOverOnWinner
        
        switch startingTurnLogic {
        case .one, .toggle, .random: startingPlayer = .one
        case .two: startingPlayer = .two
        }
        
        self.player1 = player1
        switch player1 {
        case .human:
            self.bot1 = nil
        case .bot(let level):
            self.bot1 = Bot(level: level)
        }
        
        self.player2 = player2
        switch player2 {
        case .human:
            self.bot2 = nil
        case .bot(let level):
            self.bot2 = Bot(level: level)
        }
        
        numberOfPits = (pitsPerSide + 1) * 2
        let player1StorePitID = pitsPerSide + 1
        
        // Setup pits
        typealias PitContext = (pits: [Pit], p1MovePits: [Pit], p2MovePits: [Pit])
        player2StorePit = Pit(identifier: 0, playerIdentity: .two, isStore: true)
        let pitContext: PitContext = (1..<numberOfPits).reduce(PitContext(pits: [player2StorePit], p1MovePits: [], p2MovePits: [])) { (result, pitID) in
            switch pitID {
            case 1..<player1StorePitID:
                let pit = Pit(identifier: pitID, playerIdentity: .one, isStore: false)
                return PitContext(pits: result.pits + [pit], result.p1MovePits + [pit], result.p2MovePits)
            case player1StorePitID:
                let pit = Pit(identifier: pitID, playerIdentity: .one, isStore: true)
                return PitContext(pits: result.pits + [pit], result.p1MovePits, result.p2MovePits)
            default:
                let pit = Pit(identifier: pitID, playerIdentity: .two, isStore: false)
                return PitContext(pits: result.pits + [pit], result.p1MovePits, result.p2MovePits + [pit])
            }
        }
        
        pits = pitContext.pits
        player1MovePits = pitContext.p1MovePits
        player2MovePits = pitContext.p2MovePits
        player1StorePit = pits[player1StorePitID]
        
        // Determine opposite pits
        typealias OppositePitContext = (pitMap: [Pit:Pit], oppositePitID: PitID, pits: [Pit])
        let oppositePitContext = (1..<numberOfPits).reduce(OppositePitContext(pitMap: [:], oppositePitID: numberOfPits - 1, pits: pits)) { (result, pitID) in
            guard pitID != result.oppositePitID else { return OppositePitContext(pitMap: result.pitMap, result.oppositePitID - 1, result.pits) }
            var nextPitMap = result.pitMap
            nextPitMap[result.pits[pitID]] = result.pits[result.oppositePitID]
            return OppositePitContext(pitMap: nextPitMap, result.oppositePitID - 1, result.pits)
        }
        oppositePitMap = oppositePitContext.pitMap
        
        // Setup seeds
        initialGameboardState = GameboardState(pits: pits, seedsPerPit: seedsPerPit)
        tutorialGameboardState = GameboardState(pits: pits, seedsPerPit: 4, tutorialStart: true)
        history = Stack<MoveInfo>()
        
        for pit in pits { pit.game = self }
    }
    
    deinit {
        botMoveTimer?.invalidate()
        turnTimer?.invalidate()
        countdownTimer?.invalidate()
    }
    
    public func playGame(botDelay: TimeInterval = 0) {
        guard currentGameState.isPlayable else { return }
        if isTutorialEnabled {
            reset(player1: PlayerType.human, player2: PlayerType.bot(.tutorial))
            startingPlayer = .one
        }
        makeBotMoveIfNeeded(botDelay: botDelay)
        restartTurnTimerIfNeeded()
    }
    
    public func cancelGame() {
        guard currentGameState.isPlayable else { return }
        let snapshot = GameSnapshot(gameboardState: currentGameboardState, gameState: .cancelled)
        let cancelledMoveInfo = MoveInfo(player: currentGameState.currentPlayer ?? .one, snapshot: snapshot, transitions: [])
        history.add(cancelledMoveInfo)
        stopTurnTimerIfNeeded()
    }
    
    struct BotTimerInfo {
        let movingPlayer: PlayerIdentity
        let bot: Bot
    }
    
    public func makeBotMoveIfNeeded(isTutorialMove: Bool = false, tutorialMoveDelay: TimeInterval? = nil, botDelay: TimeInterval = 0) {
        guard currentGameState.isPlayable, let movingPlayer = currentGameState.currentPlayer, let bot = bot(forPlayerIdentity: movingPlayer) else { return }
        
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            
            let waitInterval: TimeInterval
            if isTutorialMove {
                waitInterval = tutorialMoveDelay ?? 0.5
            } else {
                let delta = bot.waitRange.upperBound - bot.waitRange.lowerBound
                assert(delta > 0, "invalid bot wait internval")
                let ticks = UInt32(delta * 10)
                let tick = UInt32.random(ticks)
                waitInterval = bot.waitRange.lowerBound + (TimeInterval(tick) / 10.0) + botDelay
            }
            
            if let container = strongSelf.container {
                strongSelf.botMoveTimer = container.scheduleTimer(withTimeInterval: waitInterval, repeats: false) { [weak self] in
                    guard let strongSelf = self else { return }
                    strongSelf.makeBotMove(withPlayer: movingPlayer, bot: bot)
                }
            } else {
                strongSelf.makeBotMove(withPlayer: movingPlayer, bot: bot)
            }
        }
    }
    
    private func makeBotMove(withPlayer movingPlayer: PlayerIdentity, bot: Bot) {
        do {
            let pit = try bot.pitOfMove(forGame: self)
            let transitions = try moveTransitions(fromPit: pit)
            try makeMove(fromPit: pit)
            botDidMakeMoveHandler?(movingPlayer, GameMove(transitions: transitions, gameState: currentGameState))
            
        } catch GameBoardError.invalidGameState {
            fatalError("invalid game state")
        } catch GameBoardError.invalidPit {
            fatalError("invalid pid")
        } catch GameBoardError.invalidPlayer {
            fatalError("invalid player")
        } catch GameBoardError.invalidMoveAttempt(let message) {
            fatalError("invalid move: \(message)")
        } catch {
            fatalError("internal error")
        }
    }
    
    private func isHuman(forPlayerIdentity playerIdentity: PlayerIdentity) -> Bool {
        switch playerType(forPlayerIdentity: playerIdentity) {
        case .human: return true
        case .bot: return false
        }
    }
    
    private func playerType(forPlayerIdentity playerIdentity: PlayerIdentity) -> PlayerType {
        switch playerIdentity {
        case .one: return player1
        case .two: return player2
        }
    }
    
    private func bot(forPlayerIdentity playerIdentity: PlayerIdentity) -> Bot? {
        switch playerIdentity {
        case .one: return bot1
        case .two: return bot2
        }
    }
    
    private func oppositePit(forPit pit: Pit) -> Pit {
        guard let oppositePit = oppositePitMap[pit] else { fatalError("opposite pit not found") }
        return oppositePit
    }
    
    private func storePit(forPlayerIdentity playerIdentity: PlayerIdentity) -> Pit {
        switch playerIdentity {
        case .one: return player1StorePit
        case .two: return player2StorePit
        }
    }
    
    private func movePits(forPlayerIdentity playerIdentity: PlayerIdentity) -> [Pit] {
        switch playerIdentity {
        case .one: return player1MovePits
        case .two: return player2MovePits
        }
    }
    
    fileprivate func isValidMove(pit: Pit) -> Bool {
        guard !pit.isEmpty && !pit.isStore, let currentPlayer = currentPlayer else { return false }
        
        let playerMovePits = movePits(forPlayerIdentity: currentPlayer)
        return playerMovePits.contains(pit)
    }
    
    public var validMovePits: [Pit] {
        guard let currentPlayer = currentPlayer else { return [] }
        return movePits(forPlayerIdentity: currentPlayer).filter { !$0.isEmpty }
    }
    
    public func forceGameOverOnSkippedTurn(withWinner winner: PlayerIdentity) {
        let gameOverStartingMoves: StartingMoves
        switch currentGameState {
        case .readyToPlay:
            gameOverStartingMoves = .none
        case .inProgress(_, _, let startingMoves):
            gameOverStartingMoves = startingMoves
        case .cancelled, .gameOver:
            return
        }
        
        let gameState: GameState = .gameOver(winner: winner, reason: .skippedTurn, startingMoves: gameOverStartingMoves)
        let snapshot = GameSnapshot(gameboardState: currentGameboardState, gameState: gameState)
        let gameOverMoveInfo = MoveInfo(player: currentGameState.currentPlayer ?? .one, snapshot: snapshot, transitions: [])
        history.add(gameOverMoveInfo)
        stopTurnTimerIfNeeded()
    }
    
    public func cancelGameAndSetGamestate(_ toState: [PitID: Int]) throws {
        var fromState = currentGameboardState
        var isInTargetState = false
        while !isInTargetState {
            var didChange = false
            for pit in pits {
                let fromSeedCount: Int = fromState[pit]
                let toSeedCount: Int = toState[pit.identifier] ?? 0
                if fromSeedCount > toSeedCount {
                    let over = fromSeedCount - toSeedCount
                    for otherPit in pits {
                        let otherSeedCount = toState[otherPit.identifier] ?? 0
                        if fromState[otherPit] < otherSeedCount {
                            let under = otherSeedCount - fromState[otherPit]
                            let numberOfSeeds = min(over, under)
                            _ = try fromState.moveSeeds(from: pit, to: otherPit, numberOfSeeds: numberOfSeeds)
                            didChange = true
                            break
                        }
                    }
                    break
                }
            }
            isInTargetState = !didChange
        }
        
        let snapshot = GameSnapshot(gameboardState: fromState, gameState: .cancelled)
        let cancelledMoveInfo = MoveInfo(player: currentGameState.currentPlayer ?? .one, snapshot: snapshot, transitions: [])
        history.add(cancelledMoveInfo)
        stopTurnTimerIfNeeded()
    }
    
    private func resetTransition(for gameboardState: GameboardState) -> GameTransition? {
        do {
            var fromState = currentGameboardState
            var transition = ResetTransition()
            var isInTargetState = false
            while !isInTargetState {
                var didChange = false
                for pit in pits {
                    let fromSeedCount = fromState[pit]
                    let toSeedCount = gameboardState[pit]
                    if fromSeedCount > toSeedCount {
                        let over = fromSeedCount - toSeedCount
                        for otherPit in pits {
                            if fromState[otherPit] < gameboardState[otherPit] {
                                let under = gameboardState[otherPit] - fromState[otherPit]
                                let numberOfSeeds = min(over, under)
                                let seedTransitions = try fromState.moveSeeds(from: pit, to: otherPit, numberOfSeeds: numberOfSeeds)
                                transition.add(transitions: seedTransitions)
                                didChange = true
                                break
                            }
                        }
                        break
                    }
                }
                isInTargetState = !didChange
            }
            return transition
            
        } catch (let error) {
            assertionFailure(error.localizedDescription)
            return nil
        }
    }
    
    public var resetTransition: GameTransition? {
        return resetTransition(for: isTutorialEnabled ? tutorialGameboardState : initialGameboardState)
    }
    
    public func reset(player1: PlayerType? = nil, player2: PlayerType? = nil, gameOverOnWinner: Bool? = nil, seedsPerPit: Int? = nil) {
        stopTurnTimerIfNeeded()
        if let gameOverOnWinner = gameOverOnWinner {
            self.gameOverOnWinner = gameOverOnWinner
        }
        guard !isTutorialEnabled else {
            self.seedsPerPit = 4
            self.player1 = .human
            self.player2 = .bot(.tutorial)
            self.bot2 = Bot(level: .tutorial)
            startingPlayer = .one
            history = Stack<MoveInfo>()
            return
        }
        
        if let seedsPerPit = seedsPerPit, self.seedsPerPit != seedsPerPit {
            initialGameboardState = GameboardState(pits: pits, seedsPerPit: seedsPerPit)
            self.seedsPerPit = seedsPerPit
        }
        
        if let player1 = player1 {
            self.player1 = player1
            
            switch player1 {
            case .human:
                self.bot1 = nil
            case .bot(let level):
                self.bot1 = Bot(level: level)
            }
        }
        if let player2 = player2 {
            self.player2 = player2
            
            switch player2 {
            case .human:
                self.bot2 = nil
            case .bot(let level):
                self.bot2 = Bot(level: level)
            }
        }
        
        guard currentGameState.isDirty else {
            switch startingTurnLogic {
            case .one:
                startingPlayer = .one
            case .two:
                startingPlayer = .two
            case .random, .toggle:
                break
            }
            return
        }
        
        history = Stack<MoveInfo>()
        
        switch startingTurnLogic {
        case .one:
            startingPlayer = .one
        case .two:
            startingPlayer = .two
        case .random:
            startingPlayer = (UInt8.random(2) == 0) ? .one : .two
        case .toggle:
            startingPlayer = startingPlayer.opponentIdentity
        }
    }
    
    private func nextPit(forPit pit: Pit, playerIdentity: PlayerIdentity) -> Pit {
        return nextPit(forPit: pit, count: 1, playerIdentity: playerIdentity)
    }
    
    private func landingPit(forPit pit: Pit, playerIdentity: PlayerIdentity) -> Pit {
        return nextPit(forPit: pit, count: pit.numberOfSeeds, playerIdentity: playerIdentity)
    }
    
    private func nextPit(forPit pit: Pit, count: Int, playerIdentity: PlayerIdentity) -> Pit {
        let opponenetStorePitID = storePit(forPlayerIdentity: playerIdentity.opponentIdentity).identifier
        var nextPitID = pit.identifier
        
        switch direction {
        case .counterClockwise:
            for _ in 0 ..< count {
                switch nextPitID {
                case (numberOfPits - 1): nextPitID = opponenetStorePitID == 0 ? 1 : 0
                case opponenetStorePitID - 1: nextPitID += 2
                default: nextPitID += 1
                }
            }
            
        case .clockwise:
            for _ in 0 ..< count {
                switch nextPitID {
                case 0: nextPitID = numberOfPits - 1
                case opponenetStorePitID + 1: nextPitID -= 2
                default: nextPitID -= 1
                }
            }
        }
        
        return pits[nextPitID]
    }
    
    private func opponentLandingPits(forPit pit: Pit) -> [Pit] {
        let currentLandingPit = landingPit(forPit: pit, playerIdentity: pit.playerIdentity)
        var currentPit = nextPit(forPit: pit, playerIdentity: pit.playerIdentity)
        
        var opponentLandingPits: [Pit] = []
        while currentPit != currentLandingPit {
            if pit.playerIdentity.opponentIdentity == currentPit.playerIdentity {
                opponentLandingPits.append(currentPit)
            }
            currentPit = nextPit(forPit: currentPit, playerIdentity: pit.playerIdentity)
        }
        
        return opponentLandingPits
    }
    
    // MARK: Undo Support
    private var lastHumanMoveIndex: Int? {
        return history.ordered.index { [unowned self] in self.isHuman(forPlayerIdentity: $0.player) }
    }
    
    private var isCurrentPlayerHuman: Bool {
        guard let currentPlayer = currentPlayer else { return false }
        return isHuman(forPlayerIdentity: currentPlayer)
    }
    
    public var canUndoLastMove: Bool {
        return isCurrentPlayerHuman && (lastHumanMoveIndex != nil)
    }
    
    public func undoLastMove() -> [GameTransition]? {
        guard isCurrentPlayerHuman, let index = lastHumanMoveIndex else { return nil }
        
        return (0...index).reduce([]) { (result, _) in
            guard let result = result, let move = history.remove() else { assertionFailure("internal error: expecting history item"); return nil }
            return result + move.transitions.reversed().map { UndoTransition(playerMove: $0) }
        }
    }
    
    public func setIsTutorialEnabled(_ isTutorialEnabled: Bool) -> GameTransition? {
        let transitions = resetTransition(for: isTutorialEnabled ? tutorialGameboardState : initialGameboardState)
        self.isTutorialEnabled = isTutorialEnabled
        return transitions
    }
    
    private func moveResult(forStartingPit startingPit: Pit) -> PlayerMoveResult {
        let myIdentity = startingPit.playerIdentity
        let opponentIdentity = myIdentity.opponentIdentity
        let currentLandingPit = landingPit(forPit: startingPit, playerIdentity: myIdentity)
        
        let myStorePit = storePit(forPlayerIdentity: myIdentity)
        let opponentStorePit = storePit(forPlayerIdentity: opponentIdentity)
        
        let moveType: MoveType
        let numberOfSeedsCaptured: Int
        let numberOfSeedsBlockedFromPotentialCapture: Int
        
        if currentLandingPit == myStorePit {
            moveType = .storePit
            numberOfSeedsCaptured = 1
            numberOfSeedsBlockedFromPotentialCapture = 0
            
        } else if currentLandingPit.isEmpty && (currentLandingPit.playerIdentity == myIdentity) {
            moveType = .capture
            numberOfSeedsCaptured = 1 + oppositePit(forPit: currentLandingPit).numberOfSeeds
            
            let opponentPits = opponentLandingPits(forPit: startingPit)
            let blockedCount: Int = opponentPits.reduce(0) {
                let opponentLandingPit = landingPit(forPit: $1, playerIdentity: opponentIdentity)
                guard opponentLandingPit != myStorePit else { return 0 }
                return opponentLandingPit == opponentStorePit ? $0 + 1 : $0 + 1 + oppositePit(forPit: opponentLandingPit).numberOfSeeds
            }
            numberOfSeedsBlockedFromPotentialCapture = blockedCount
            
        } else {
            moveType = .normal
            numberOfSeedsCaptured = 0
            numberOfSeedsBlockedFromPotentialCapture = 0
        }
        
        return PlayerMoveResult(startingPit: startingPit,
                                landingPit: currentLandingPit,
                                moveType: moveType,
                                numberOfSeedsCaptured: numberOfSeedsCaptured,
                                numberOfSeedsBlockedFromPotentialCapture: numberOfSeedsBlockedFromPotentialCapture)
    }
    
    // helpful for bots
    var nextMoveResultsForCurrentPlayer: [PlayerMoveResult] {
        guard let currentPlayer = currentPlayer else { return [] }
        return movePits(forPlayerIdentity: currentPlayer).compactMap { $0.isEmpty ? nil : moveResult(forStartingPit: $0) }
    }
    
    public func pit(forPitID pitID: PitID) throws -> Pit {
        guard pitID >= 0 && pitID < numberOfPits else { throw GameBoardError.invalidPit }
        return pits[pitID]
    }
    
    public func previewTransitions(fromPit: Pit) throws -> [GameTransition] {
        let move = try moveInfoForPit(fromPit: fromPit, preview: true)
        return move.transitions
    }
    
    public func moveTransitions(fromPitID: PitID) throws -> [GameTransition] {
        let fromPit = try pit(forPitID: fromPitID)
        return try moveTransitions(fromPit: fromPit)
    }
    
    public func moveTransitions(fromPit: Pit) throws -> [GameTransition] {
        let move = try moveInfoForPit(fromPit: fromPit, preview: false)
        return move.transitions
    }
    
    public func makeMove(fromPitID: PitID) throws {
        let fromPit = try pit(forPitID: fromPitID)
        try makeMove(fromPit: fromPit)
    }
    
    public func makeMove(fromPit: Pit) throws {
        restartTurnTimerIfNeeded()
        let move = try moveInfoForPit(fromPit: fromPit, preview: false)
        history.add(move)
    }
    
    public func stopTurnTimerIfNeeded() {
        turnTimer?.invalidate()
        botMoveTimer?.invalidate()
        countdownTimer?.invalidate()
        
        turnTimer = nil
        botMoveTimer = nil
        countdownTimer = nil
    }
    
    public func restartTurnTimerIfNeeded() {
        guard let turnTimeLimit = turnTimeLimit, currentGameState.isPlayable, !isTutorialEnabled else { return }
        turnTimer?.invalidate()
        countdownTimer?.invalidate()
        
        var countdownStartInterval: TimeInterval? = nil
        if let countdownTime = countdownTime, countdownTime + 3.0 < turnTimeLimit {
            countdownStartInterval = turnTimeLimit - countdownTime
        }
        
        if let container = container {
            turnTimer = container.scheduleTimer(withTimeInterval: turnTimeLimit, repeats: false) { [weak self] in
                self?.onTurnTimeout()
            }
            if let countdownStartInterval = countdownStartInterval {
                countdownTimer = container.scheduleTimer(withTimeInterval: countdownStartInterval, repeats: false) { [weak self] in
                    self?.onTurnCountdown()
                }
            }
        } else {
            let item = DispatchQueue.main.addOperation(turnTimeLimit) { [weak self] in
                self?.onTurnTimeout()
            }
            turnTimer = WorkItemTimer(item: item, timeInterval: turnTimeLimit)
            
            if let countdownStartInterval = countdownStartInterval {
                let countdownItem = DispatchQueue.main.addOperation(countdownStartInterval) { [weak self] in
                    self?.onTurnCountdown()
                }
                countdownTimer = WorkItemTimer(item: countdownItem, timeInterval: countdownStartInterval)
            }
        }
    }
    
    private func skipTurn(performTimeoutCallback: Bool) {
        stopTurnTimerIfNeeded()
        
        let slowPlayer: PlayerIdentity
        let currentWinner: PlayerIdentity?
        let startingMoves: StartingMoves
        switch currentGameState {
        case .readyToPlay(let currentPlayer):
            slowPlayer = currentPlayer
            currentWinner = nil
            startingMoves = .normal
        case .inProgress(let currentPlayer, let winner, _):
            slowPlayer = currentPlayer
            currentWinner = winner
            startingMoves = .normal
        case .gameOver, .cancelled:
            return
        }
        
        let nextPlayer = slowPlayer.opponentIdentity
        let nextGameState = GameState.inProgress(currentPlayer: nextPlayer, winner: currentWinner, startingMoves: startingMoves)
        let snapshot = GameSnapshot(gameboardState: currentGameboardState, gameState: nextGameState)
        let move = MoveInfo(nextPlayer, snapshot: snapshot, transitions: [])
        history.add(move)
        
        if performTimeoutCallback {
            playerDidTimeoutTurnHandler?(slowPlayer, currentGameState)
        }
        restartTurnTimerIfNeeded()
    }
    
    public func skipTurn() {
        skipTurn(performTimeoutCallback: false)
    }
    
    private func onTurnTimeout() {
        skipTurn(performTimeoutCallback: true)
    }
    
    private func onTurnCountdown() {
        guard let countdownTime = countdownTime else { return }
        let slowPlayer: PlayerIdentity
        switch currentGameState {
        case .readyToPlay(let currentPlayer):
            slowPlayer = currentPlayer
        case .inProgress(let currentPlayer, _, _):
            slowPlayer = currentPlayer
        case .gameOver, .cancelled:
            return
        }
        
        let seconds = Int(round(countdownTime))
        playerDidStartTurnCountdownHandler?(slowPlayer, seconds)
    }
    
    private func moveInfoForPit(fromPit: Pit, preview: Bool) throws -> MoveInfo {
        guard let currentPlayer = currentPlayer else { throw GameBoardError.invalidGameState }
        guard fromPit.isValidMove else { throw GameBoardError.invalidPit }
        
        // make moves
        typealias MoveContext = (state: GameboardState, playerTransition: MoveTransition, lastPit: Pit)
        let playerTransition = MoveTransition(fromPit: fromPit, moveType: preview ? .preview : .normal)
        let moveContext: MoveContext = try (0 ..< fromPit.numberOfSeeds).reduce(MoveContext(currentGameboardState, playerTransition, fromPit)) { (result, _) in
            let nextLastPit = nextPit(forPit: result.lastPit, playerIdentity: currentPlayer)
            //            guard nextLastPit != fromPit else { return result }
            var nextState = result.state
            let seedTransition = try nextState.moveSingleSeed(from: fromPit, to: nextLastPit)
            return MoveContext(state: nextState, playerTransition: result.playerTransition.added(transition: seedTransition), lastPit: nextLastPit)
        }
        var transitions: [GameTransition] = [moveContext.playerTransition]
        let lastPit = moveContext.lastPit
        var nextGameboardState = moveContext.state
        
        let nextGameState: GameState
        var nextPlayer = currentPlayer
        let lastPitSeedCount = nextGameboardState[lastPit]
        
        // determine move event type
        // check for last move in current player's store pit
        if lastPit == storePit(forPlayerIdentity: currentPlayer) {
            //lastPlayerMoveType = .StorePit
            
        } else if (lastPit.playerIdentity == currentPlayer) && !nextGameboardState.arePitsEmpty([oppositePit(forPit: lastPit)]) && (lastPitSeedCount == 1) {
            let myStorePit = storePit(forPlayerIdentity: currentPlayer)
            let capturePit = oppositePit(forPit: lastPit)
            
            var captureTransition = MoveTransition(fromPit: capturePit, moveType: .capture)
            let opponentSeedTransitions = try nextGameboardState.moveAllSeeds(from: capturePit, to: myStorePit)
            captureTransition.add(transitions: opponentSeedTransitions)
            
            // now move the capturing seed to the store as well
            // update interals
            let mySeedTransition = try nextGameboardState.moveSingleSeed(from: lastPit, to: myStorePit)
            captureTransition.add(transition: mySeedTransition)
            
            transitions.append(captureTransition)
            nextPlayer = currentPlayer.opponentIdentity
            
        } else {
            nextPlayer = currentPlayer.opponentIdentity
        }
        
        // Look to see if the game is over
        let gameOverIdentityToCleanUp: PlayerIdentity?
        if nextGameboardState.arePitsEmpty(movePits(forPlayerIdentity: .one)) {
            gameOverIdentityToCleanUp = .two
        } else if nextGameboardState.arePitsEmpty(movePits(forPlayerIdentity: .two)) {
            gameOverIdentityToCleanUp = .one
        } else {
            gameOverIdentityToCleanUp = nil
        }
        
        if let gameOverIdentityToCleanUp = gameOverIdentityToCleanUp {
            // if game is over, then set game over info like winner and tie stuff
            // also move any not-store seeds into their respective player's store
            
            // update internals
            let storePitForCleanUp = storePit(forPlayerIdentity: gameOverIdentityToCleanUp)
            var gameOverTransition = GameOverTransition()
            let seedTransitions = try movePits(forPlayerIdentity: gameOverIdentityToCleanUp).reduce([]) {
                nextGameboardState[$1] > 0 ? try $0 + nextGameboardState.moveAllSeeds(from: $1, to: storePitForCleanUp) : $0
            }
            gameOverTransition.add(transitions: seedTransitions)
            transitions.append(gameOverTransition)
            
            // set "winner" and "is tie" game stuff here
            let p1SeedCount = nextGameboardState[player1StorePit]
            let p2SeedCount = nextGameboardState[player2StorePit]
            let winner: PlayerIdentity? = (p1SeedCount > p2SeedCount) ? .one : (p2SeedCount > p1SeedCount) ? .two : nil
            
            var startingMoves: StartingMoves = .normal
            switch currentGameState {
            case .inProgress(_, _, let currentStartingMoves):
                startingMoves = currentStartingMoves
            case .gameOver(_, _, let currentStartingMoves):
                startingMoves = currentStartingMoves
            default:
                ()
            }
            nextGameState = .gameOver(winner: winner, reason: .completedGame, startingMoves: startingMoves)
            
        } else {
            let winner: PlayerIdentity?
            let winningNumberOfSeeds: Int = (seedsPerPit * (numberOfPits - 2)) / 2
            if nextGameboardState[player1StorePit] > winningNumberOfSeeds {
                winner = .one
            } else if nextGameboardState[player2StorePit] > winningNumberOfSeeds {
                winner = .two
            } else {
                winner = nil
            }
            
            var startingMoves: StartingMoves = .normal
            switch currentGameState {
            case .readyToPlay:
                if let firstPitID = firstNonNormalPitID(for: startingPlayer), fromPit.identifier == firstPitID {
                    startingMoves = .four
                } else {
                    startingMoves = .normal
                }
            case .inProgress(_, _, let currentStartingMoves):
                if currentStartingMoves.isFour {
                    let isFourOne = currentPlayer == startingPlayer && fromPit.identifier == secondNonNormalPitID(for: startingPlayer)
                    startingMoves = isFourOne ? .fourOne : .normal
                } else {
                    startingMoves = currentStartingMoves
                }
            case .gameOver(_, _, let currentStartingMoves):
                startingMoves = currentStartingMoves
            case .cancelled:
                ()
            }
            
            if gameOverOnWinner, let winner = winner {
                nextGameState = .gameOver(winner: winner, reason: .determinedWinner, startingMoves: startingMoves)
            } else {
                nextGameState = .inProgress(currentPlayer: nextPlayer, winner: winner, startingMoves: startingMoves)
            }
        }
        
        let snapshot = GameSnapshot(gameboardState: nextGameboardState, gameState: nextGameState)
        let moveInfo = MoveInfo(currentPlayer, snapshot: snapshot, transitions: transitions)
        return moveInfo
    }
    
    private func firstNonNormalPitID(for playerIdentity: PlayerIdentity) -> PitID? {
        guard pitsPerSide >= seedsPerPit, seedsPerPit > 1 else { return nil }
        switch playerIdentity {
        case .one:
            return (pitsPerSide + 1) - seedsPerPit
        case .two:
            return (pitsPerSide * 2) - seedsPerPit
        }
    }
    
    private func secondNonNormalPitID(for playerIdentity: PlayerIdentity) -> PitID {
        switch playerIdentity {
        case .one:
            return pitsPerSide
        case .two:
            return (pitsPerSide * 2) + 1
        }
    }

    private func debugDescription(pit: Pit, state: GameboardState) -> String {
        let seedCount = state[pit]
        return seedCount < 10 ? "[ \(seedCount)]" : "[\(seedCount)]"
    }
    
    private func debugDescription(forState state: GameboardState) -> String {
        let margin = 5
        let hEdgeChar = "-"
        let spaceChar = " "
        let vEdge = "|"
        let edgeCount = ((pitsPerSide * 5) - 1) + (2 * (margin + 1))
        let hEdge = String(repeating: hEdgeChar, count: edgeCount)
        let leftMargin = vEdge + String(repeating: spaceChar, count: margin)
        let rightMargin = String(repeating: spaceChar, count: margin) + vEdge
        
        var p1Buffer = ""
        for pit in player1MovePits {
            if !p1Buffer.isEmpty {
                p1Buffer += " "
            }
            p1Buffer += debugDescription(pit: pit, state: state)
        }
        
        var p2Buffer = ""
        let p2Pits = player2MovePits.sorted { $0.identifier > $1.identifier }
        for pit in p2Pits {
            if !p2Buffer.isEmpty {
                p2Buffer += " "
            }
            p2Buffer += debugDescription(pit: pit, state: state)
        }
        
        let midBuffer = " \(debugDescription(pit: player2StorePit, state: state))" +
            String(repeating: " ", count: p1Buffer.count - 1) + " \(debugDescription(pit: player1StorePit, state: state)) "
        
        let topLine = leftMargin + p2Buffer + rightMargin
        let midLine = vEdge + midBuffer + vEdge
        let bottomLine = leftMargin + p1Buffer + rightMargin
        let stateLine = "\(currentGameState)"
        
        let buffer = "\(stateLine)\n\(hEdge)\n\(topLine)\n\(midLine)\n\(bottomLine)\n\(hEdge)\n"
        return buffer
    }
    
    public var debugDescription: String {
        return debugDescription(forState: currentGameboardState)
    }
    
    fileprivate struct GameboardState {
        var pitSeedCountMap: [Pit: SeedStack] = [:]
        
        init(pits: [Pit], seedsPerPit: Int, tutorialStart: Bool = false) {
            
            func numberOfSeedsForPit(_ pit: Pit, isTutorial: Bool) -> Int {
                if isTutorial {
                    switch pit.identifier {
                    case 0: return 22
                    case 1: return 1
                    case 3: return 3
                    case 7: return 13
                    case 10: return 0
                    case 11: return 1
                    case 12: return 8
                    case 13: return 0
                    default: return 0
                    }
                } else {
                    return pit.isStore ? 0 : seedsPerPit
                }
            }
            
            var tutorialMode = false
            if tutorialStart {
                tutorialMode = pits.count == 14 && seedsPerPit == 4
                assert(tutorialMode, "gameboard setup does not support a tutorial mode")
            }
            
            let intialState: ([Pit:SeedStack], Int) = pits.reduce(([:], 0)) { (result, pit) in
                var map = result.0
                let seedID = result.1
                let numberOfSeedsInThisPit = numberOfSeedsForPit(pit, isTutorial: tutorialMode)
                map[pit] = SeedStack((0 ..< numberOfSeedsInThisPit).map { $0 + seedID })
                return (map, seedID + numberOfSeedsInThisPit)
            }
            pitSeedCountMap = intialState.0
        }
        
        subscript (key: Pit) -> Int {
            get {
                guard let seeds = pitSeedCountMap[key] else { assert(false, "invalid pit. this should never happen"); return 0 }
                return seeds.count
            }
        }
        
        mutating func moveAllSeeds(from fromPit: Pit, to toPit: Pit) throws -> [SeedTransition] {
            return try moveSeeds(from: fromPit, to: toPit, numberOfSeeds: self[fromPit])
        }
        
        mutating func moveSeeds(from fromPit: Pit, to toPit: Pit, numberOfSeeds: Int) throws -> [SeedTransition] {
            guard numberOfSeeds > 0 else { throw GameBoardError.invalidMoveAttempt(message: "attempt to move where there aren't any seeds") }
            guard numberOfSeeds <= self[fromPit] else { throw GameBoardError.invalidMoveAttempt(message: "attempt to move too many seeds") }
            guard self[fromPit] > 0 else { throw GameBoardError.invalidMoveAttempt(message: "attempt to move empty pit") }
            guard let fromSeeds = pitSeedCountMap[fromPit] else { throw GameBoardError.invalidMoveAttempt(message: "internal error: from pit info is nil") }
            
            guard fromPit != toPit else {
                let transitions = fromSeeds.ordered[0 ..< numberOfSeeds].reduce([]) { (result, seedID) in
                    return result + [SeedTransition(seedID: seedID, fromPit: fromPit, toPit: fromPit)]
                }
                guard transitions.count == numberOfSeeds else { throw GameBoardError.invalidMoveAttempt(message: "internal error: stationary move is corrupt") }
                return transitions
            }
            
            typealias MoveContext = (movableSeeds: SeedStack, transitions: [SeedTransition])
            let moveContext: MoveContext = try (0..<numberOfSeeds).reduce(MoveContext(fromSeeds, [])) { (result, _) in
                guard let toSeeds = pitSeedCountMap[toPit] else { throw GameBoardError.invalidMoveAttempt(message: "internal error. to pit info is nil") }
                let (nextMovableSeeds, movableSeedID) = result.movableSeeds.removed()
                guard let seedID = movableSeedID else { throw GameBoardError.invalidMoveAttempt(message: "Internal error. movable seed id is nil") }
                pitSeedCountMap[toPit] = toSeeds.added(seedID)
                return MoveContext(movableSeeds: nextMovableSeeds, transitions: result.transitions + [SeedTransition(seedID: seedID, fromPit: fromPit, toPit: toPit)])
            }
            pitSeedCountMap[fromPit] = moveContext.movableSeeds
            return moveContext.transitions
        }
        
        mutating func moveSingleSeed(from fromPit: Pit, to toPit: Pit) throws -> SeedTransition {
            let transitions = try moveSeeds(from: fromPit, to: toPit, numberOfSeeds: 1)
            guard let transition = transitions.first else { throw GameBoardError.invalidMoveAttempt(message: "internal error on single seed move") }
            return transition
        }
        
        func arePitsEmpty(_ pits: [Pit]) -> Bool {
            return !pits.contains { self[$0] > 0 }
        }
        
        func isEmpty(pit: Pit) -> Bool {
            return self[pit] == 0
        }
    }
}

public func ==(lhs: Pit, rhs: Pit) -> Bool {
    return lhs.identifier == rhs.identifier
}

// MARK: Transitions
private protocol WritableTransition {
    var seedTransitions: [SeedTransition] { get set }
}

private extension WritableTransition {
    mutating func add(transitions: [SeedTransition]) {
        seedTransitions += transitions
    }
    
    mutating func add(transition: SeedTransition) {
        seedTransitions.append(transition)
    }
    
    func added(transition: SeedTransition) -> Self {
        var addedTransition = self
        addedTransition.add(transition: transition)
        return addedTransition
    }
    
    func added(transitions: [SeedTransition]) -> Self {
        var addedTransition = self
        addedTransition.add(transitions: transitions)
        return addedTransition
    }
}

struct MoveTransition: GameTransition, WritableTransition {
    fileprivate(set) var seedTransitions: [SeedTransition]
    
    let fromPit: Pit
    let moveType: MoveType
    let moveInGroupHint: Bool
    let canUndo = true
    
    fileprivate init(fromPit: Pit, moveType: MoveType) {
        self.fromPit = fromPit
        self.moveType = moveType
        seedTransitions = []
        
        switch moveType {
        case .normal: moveInGroupHint = false
        default: moveInGroupHint = true
        }
    }
}

struct ResetTransition: GameTransition, WritableTransition {
    let moveType: MoveType = .reset
    fileprivate(set) var seedTransitions: [SeedTransition] = []
    let moveInGroupHint = true
    let canUndo = false
}

struct GameOverTransition: GameTransition, WritableTransition {
    let moveType: MoveType  = .storePit
    fileprivate(set) var seedTransitions: [SeedTransition] = []
    let moveInGroupHint = true
    let canUndo = false
}

struct WorkItemTimer: GameTimerProtocol {
    let item: DispatchWorkItem
    let timeInterval: TimeInterval
    
    var fireDate: Date {
        return Date()
    }
    
    var isValid: Bool {
        return !item.isCancelled
    }
    
    func invalidate() {
        item.cancel()
    }
    
    func fireIfNeeded() {
    }
}

extension PlayerIdentity {
    var firstNonNormalPitID: PitID {
        switch self {
        case .one:
            return 3
        case .two:
            return 10
        }
    }
    var secondNonNormalPitID: PitID {
        switch self {
        case .one:
            return 6
        case .two:
            return 13
        }
    }
}
