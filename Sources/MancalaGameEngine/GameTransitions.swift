//
//  PlayerMove.swift
//
//  Created by Sanford, Michael on 6/20/15.
//  Copyright © 2015 Sanford, Michael. All rights reserved.
//

import Foundation

public protocol GameTransition {
    var moveType: MoveType { get }
    var seedTransitions: [SeedTransition] { get }
    var moveInGroupHint: Bool { get }
    var canUndo: Bool { get }
}

public struct SeedTransition {
    public let seedID: SeedID
    public let fromPit: Pit
    public let toPit: Pit
}

/*-------------------------------------------*/

public struct UndoTransition: GameTransition {
    public let moveType: MoveType
    public let seedTransitions: [SeedTransition]
    public let moveInGroupHint: Bool = true
    public let canUndo: Bool = false
    
    public init(playerMove: GameTransition) {
        seedTransitions = playerMove.seedTransitions.reversed().map { SeedTransition(seedID: $0.seedID, fromPit: $0.toPit, toPit: $0.fromPit) }
        moveType = .undo
    }
}

public struct SkipTransition: GameTransition {
    public let moveType: MoveType = .skip
    public let seedTransitions: [SeedTransition] = []
    public let moveInGroupHint: Bool = false
    public let canUndo: Bool = true
    
    public init() {}
}
