//
//  MoveInfo.swift
//  mac
//
//  Created by Sanford, Michael on 6/21/15.
//  Copyright © 2015 Sanford, Michael. All rights reserved.
//

import Foundation

struct PlayerMoveResult {
    let startingPit: Pit
    let landingPit: Pit
    let moveType: MoveType
    let numberOfSeedsCaptured: Int
    let numberOfSeedsBlockedFromPotentialCapture: Int
}
