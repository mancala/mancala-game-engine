//
//  Bot.swift
//  Mancala
//
//  Created by Michael Sanford on 6/26/15.
//  Copyright © 2016 flipside5. All rights reserved.
//

import Foundation
import MancalaCore
import NetworkingCore

public class Bot {
    private enum MoveQuality {
        case bad, good, great
    }
    
    public let level: BotLevel
    
    public init(level: BotLevel) {
        self.level = level
    }
    
    public var waitRange: Range<TimeInterval> {
        switch level {
        case .silly, .beginner:
            return Range(uncheckedBounds: (1.0, 2.0))
        case .intermediate:
            return Range(uncheckedBounds: (1.0, 2.0))
        case .advanced, .master, .tutorial:
            return Range(uncheckedBounds: (1.0, 2.0))
        }
    }
    
    public func pitOfMove(forGame game: Game) throws -> Pit {
        switch level {
        case .silly: return try sillyMove(forGame: game)
        case .beginner: return try randomPit(fromPits: game.validMovePits)
        case .intermediate: return try reasonableMove(forGame: game, moveQuality: .bad)
        case .advanced: return try reasonableMove(forGame: game, moveQuality: .good)
        case .master: return try reasonableMove(forGame: game, moveQuality: .great)
        case .tutorial: return try tutorialMove(forGame: game)
        }
    }
    
    private func tutorialMove(forGame game: Game) throws -> Pit {
        let moves = game.nextMoveResultsForCurrentPlayer
        guard let firstMove = moves.first else { throw GameBoardError.invalidGameState }
        let nextMovePit: Pit = moves.reduce(firstMove.startingPit) { result, moveResult in
            guard moveResult.startingPit.identifier < result.identifier else { return result }
            return moveResult.startingPit
        }
        return nextMovePit
    }
    
    private func reasonableMove(forGame game: Game, moveQuality: MoveQuality) throws -> Pit {
        let moves = game.nextMoveResultsForCurrentPlayer
        guard moves.count > 0 else { throw GameBoardError.invalidGameState }
        
        struct MoveInfo {
            private(set) var offensivePits: [Pit] = []
            private(set) var defensivePits: [Pit] = []
            private(set) var maxNumberOfSeedsCaptured: Int = 0
            private(set) var maxNumberOfSeedsBlocked: Int = 0
            
            func bestPits(forMoveQuality moveQuality: MoveQuality) -> [Pit] {
                let makeDumbMove: Bool
                switch moveQuality {
                case .bad: makeDumbMove = UInt8.random(2) == 0
                case .good: makeDumbMove = UInt8.random(8) == 0
                case .great: makeDumbMove = false
                }
                
                let intersectingPits: [Pit] = makeDumbMove ? [] : offensivePits.filter { defensivePits.contains($0) }
                return intersectingPits.isEmpty ? (offensivePits.isEmpty ? defensivePits : offensivePits) : intersectingPits
            }
            
            func betterMoveInfo(withMove move: PlayerMoveResult) -> MoveInfo {
                var newMoveInfo = self
                if move.numberOfSeedsCaptured == maxNumberOfSeedsCaptured {
                    newMoveInfo.offensivePits.append(move.startingPit)
                } else if move.numberOfSeedsCaptured > maxNumberOfSeedsCaptured {
                    newMoveInfo.offensivePits = [move.startingPit]
                    newMoveInfo.maxNumberOfSeedsCaptured = move.numberOfSeedsCaptured
                }
                
                if move.numberOfSeedsBlockedFromPotentialCapture == maxNumberOfSeedsCaptured {
                    newMoveInfo.defensivePits.append(move.startingPit)
                } else if move.numberOfSeedsBlockedFromPotentialCapture > maxNumberOfSeedsBlocked {
                    newMoveInfo.defensivePits = [move.startingPit]
                    newMoveInfo.maxNumberOfSeedsBlocked = move.numberOfSeedsBlockedFromPotentialCapture
                }
                return newMoveInfo
            }
        }
        
        let goodMoveInfo = moves.reduce(MoveInfo()) { (result, move) in result.betterMoveInfo(withMove: move) }
        return goodMoveInfo.bestPits(forMoveQuality: moveQuality).last!
    }
    
    private func sillyMove(forGame game: Game) throws -> Pit {
        guard let moveResult = game.nextMoveResultsForCurrentPlayer.last else { throw GameBoardError.invalidGameState }
        return moveResult.startingPit
    }
    
    private func randomPit(fromPits pits: [Pit]) throws -> Pit {
        guard !pits.isEmpty else { throw GameBoardError.invalidGameState }
        let index = PitID(UInt32.random(UInt32(pits.count)))
        return pits[index]
    }
}
