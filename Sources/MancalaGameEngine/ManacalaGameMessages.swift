//
//  ManacalaMessages.swift
//  manserver
//
//  Created by Michael Sanford on 12/22/16.
//  Copyright © 2016 flipside5. All rights reserved.
//

import Foundation
import NetworkingCore
import GamingCore
import MancalaCore

public enum MancalaClientToGameServerMessageType: MessageType {
    case playerDidRequestMove = 32
    case playerDidRequestToPlayAgain = 33
    case playerDidInteractWithProp = 34
    case playerDidSkipTurn = 35
    case playerDidSendChatMessage = 36
}

public enum MancalaGameServerToClientMessageType: MessageType {
    case gameDidStart = 64
    case playerDidMakeMove = 65
    case playerDidSkipTurn = 66
    case playerDidJoinGame = 67
    case playerDidLeaveGame = 68
    case playerDidInteractWithProp = 69
    case playerDidStartTurnCountdown = 70
    case playerDidEarnPointsAndCoins = 71
    case playerDidSendChatMessage = 72
}

public enum MancalaNetworkProtocolVersion: Int {
    case v1_0
    case v1_1
    
    static let latest: MancalaNetworkProtocolVersion = .v1_1
    static var current: MancalaNetworkProtocolVersion = .latest
}

public func >=(lhs: MancalaNetworkProtocolVersion, rhs: MancalaNetworkProtocolVersion) -> Bool {
    return lhs.rawValue >= rhs.rawValue
}

/*-----------------------------------------*/

// MARK: Client --> Server

public struct PlayerDidSendChatPayload: ByteCoding {
    public static let clientToServerType = MancalaClientToGameServerMessageType.playerDidSendChatMessage
    public static let serverToClientType = MancalaGameServerToClientMessageType.playerDidSendChatMessage
    
    public let fromPlayer: PlayerIdentity
    public let message: String
    
    public init(fromPlayer: PlayerIdentity, message: String) {
        self.fromPlayer = fromPlayer
        self.message = message
    }
    
    public init?(message: Message) {
        guard message.type == PlayerDidSendChatPayload.serverToClientType.rawValue, let payload = message.payload else {
            assert(false, "failed to unarchive PlayerDidSendChatPayload due to bad format")
            return nil
        }
        
        let unarchiver = ByteUnarchiver(archive: payload)
        self.init(unarchiver: unarchiver)
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        guard let playerIdentity = PlayerIdentity(unarchiver: unarchiver) else { return nil }
        do {
            self.fromPlayer = playerIdentity
            self.message = try unarchiver.decodeString()
        } catch {
            assert(false, "failed to unarchive PlayerDidSendChatPayload")
            return nil
        }
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(fromPlayer)
        archiver.encode(message)
    }
}

public struct PlayerDidInteractWithPropMessage: ByteCoding {
    public static let clientToServerType = MancalaClientToGameServerMessageType.playerDidInteractWithProp
    public static let serverToClientType = MancalaGameServerToClientMessageType.playerDidInteractWithProp
    
    public let playerIdentity: PlayerIdentity
    public let propID: String
    public let interactionType: Int
    
    public init(playerIdentity: PlayerIdentity, propID: String, interactionType: Int = 0) {
        self.playerIdentity = playerIdentity
        self.propID = propID
        self.interactionType = interactionType
    }
    
    public init?(message: Message) {
        guard message.type == PlayerDidInteractWithPropMessage.serverToClientType.rawValue, let payload = message.payload else {
            assert(false, "failed to unarchive PlayerDidInteractWithPropMessage due to bad format")
            return nil
        }
        
        let unarchiver = ByteUnarchiver(archive: payload)
        self.init(unarchiver: unarchiver)
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        guard let playerIdentity = PlayerIdentity(unarchiver: unarchiver) else { return nil }
        do {
            self.playerIdentity = playerIdentity
            propID = try unarchiver.decodeString()
            interactionType = try unarchiver.decodeInt()
        } catch {
            assert(false, "failed to unarchive PlayerDidInteractWithPropMessage")
            return nil
        }
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(playerIdentity)
        archiver.encode(propID)
        archiver.encode(interactionType)
    }
}

public struct PlayerDidRequestMoveMessage: ByteCoding {
    public static let type = MancalaClientToGameServerMessageType.playerDidRequestMove
    
    public let playerIdentity: PlayerIdentity
    public let pitID: PitID
    
    public init(playerIdentity: PlayerIdentity, pitID: PitID) {
        self.playerIdentity = playerIdentity
        self.pitID = pitID
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        guard let playerIdentity = PlayerIdentity.init(unarchiver: unarchiver) else { return nil }
        do {
            self.playerIdentity = playerIdentity
            pitID = try unarchiver.decodePitID()
        } catch {
            assert(false, "failed to unarchive PlayerRequestsMoveMessage")
            return nil
        }
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(playerIdentity)
        archiver.encode(pitID: pitID)
    }
}

/*-----------------------------------------*/

public struct PlayerDidRequestToPlayAgainMessage: ByteCoding {
    public static let type = MancalaClientToGameServerMessageType.playerDidRequestToPlayAgain
    
    public let playerIdentity: PlayerIdentity
    
    public init(playerIdentity: PlayerIdentity) {
        self.playerIdentity = playerIdentity
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        guard let playerIdentity = PlayerIdentity.init(unarchiver: unarchiver) else { return nil }
        self.playerIdentity = playerIdentity
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(playerIdentity)
    }
}

/*-----------------------------------------*/

// MARK: Server --> Client
public struct PlayerDidJoinGameMessage: ByteCoding {
    public static let type = MancalaGameServerToClientMessageType.playerDidJoinGame
    
    public let playerIdentity: PlayerIdentity
    
    public init(playerIdentity: PlayerIdentity) {
        self.playerIdentity = playerIdentity
    }
    
    public init?(message: Message) {
        guard message.type == PlayerDidJoinGameMessage.type.rawValue, let payload = message.payload else {
            assert(false, "failed to unarchive PlayerDidJoinGame due to bad format")
            return nil
        }
        
        let unarchiver = ByteUnarchiver(archive: payload)
        self.init(unarchiver: unarchiver)
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        guard let playerIdentity = PlayerIdentity.init(unarchiver: unarchiver) else { return nil }
        self.playerIdentity = playerIdentity
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(playerIdentity)
    }
}

/*-----------------------------------------*/

public struct PlayerDidLeaveGameMessage: ByteCoding {
    public static let type = MancalaGameServerToClientMessageType.playerDidLeaveGame
    
    public let playerIdentity: PlayerIdentity
    
    public init(playerIdentity: PlayerIdentity) {
        self.playerIdentity = playerIdentity
    }
    
    public init?(message: Message) {
        guard message.type == PlayerDidLeaveGameMessage.type.rawValue, let payload = message.payload else {
            assert(false, "failed to unarchive PlayerDidLeaveGameMessage due to bad format")
            return nil
        }
        
        let unarchiver = ByteUnarchiver(archive: payload)
        self.init(unarchiver: unarchiver)
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        guard let playerIdentity = PlayerIdentity.init(unarchiver: unarchiver) else { return nil }
        self.playerIdentity = playerIdentity
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(playerIdentity)
    }
}

/*-----------------------------------------*/

public struct PlayerDidMakeMoveMessage: ByteCoding {
    public static let type = MancalaGameServerToClientMessageType.playerDidMakeMove
    
    public let playerIdentity: PlayerIdentity
    public let pitID: PitID
    
    public init(playerIdentity: PlayerIdentity, pitID: PitID) {
        self.playerIdentity = playerIdentity
        self.pitID = pitID
    }
    
    public init?(message: Message) {
        guard message.type == PlayerDidMakeMoveMessage.type.rawValue, let payload = message.payload else {
            assert(false, "failed to unarchive PlayerDidMakeMoveMessage due to bad format")
            return nil
        }
        
        let unarchiver = ByteUnarchiver(archive: payload)
        self.init(unarchiver: unarchiver)
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        guard let playerIdentity = PlayerIdentity(unarchiver: unarchiver) else {
            assert(false, "failed to unarchive PlayerDidMakeMoveMessage due to bad format")
            return nil
        }
        
        do {
            self.playerIdentity = playerIdentity
            pitID = try unarchiver.decodePitID()
        } catch {
            assert(false, "failed to unarchive PlayerRequestsMoveMessage")
            return nil
        }
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(playerIdentity)
        archiver.encode(pitID: pitID)
    }
}

/*-----------------------------------------*/

public struct PlayerDidSkipTurn: ByteCoding {
    public static let clientToServerType = MancalaClientToGameServerMessageType.playerDidSkipTurn
    public static let serverToClientType = MancalaGameServerToClientMessageType.playerDidSkipTurn
    
    public let playerIdentity: PlayerIdentity
    
    public init(playerIdentity: PlayerIdentity) {
        self.playerIdentity = playerIdentity
    }
    
    public init?(message: Message) {
        guard message.type == PlayerDidSkipTurn.serverToClientType.rawValue, let payload = message.payload else {
            assert(false, "failed to unarchive PlayerDidSkipTurn due to bad format")
            return nil
        }
        
        let unarchiver = ByteUnarchiver(archive: payload)
        self.init(unarchiver: unarchiver)
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        guard let playerIdentity = PlayerIdentity(unarchiver: unarchiver) else {
            assert(false, "failed to unarchive PlayerDidSkipTurn due to bad format")
            return nil
        }
        
        self.playerIdentity = playerIdentity
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(playerIdentity)
    }
}

/*-----------------------------------------*/

public struct PlayerDidStartTurnCountdown: ByteCoding {
    public static let type = MancalaGameServerToClientMessageType.playerDidStartTurnCountdown
    
    public let playerIdentity: PlayerIdentity
    public let seconds: Int
    
    public init(playerIdentity: PlayerIdentity, seconds: Int) {
        self.playerIdentity = playerIdentity
        self.seconds = seconds
    }
    
    public init?(message: Message) {
        guard message.type == PlayerDidStartTurnCountdown.type.rawValue, let payload = message.payload else {
            assert(false, "failed to unarchive PlayerDidStartTurnCountdown due to bad format")
            return nil
        }
        
        let unarchiver = ByteUnarchiver(archive: payload)
        self.init(unarchiver: unarchiver)
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        guard let playerIdentity = PlayerIdentity(unarchiver: unarchiver) else {
            assert(false, "failed to unarchive PlayerDidStartTurnCountdown due to bad format")
            return nil
        }
        
        self.playerIdentity = playerIdentity
        do {
            self.seconds = Int(try unarchiver.decodeUInt8())
        } catch {
            assert(false, "failed to unarchive PlayerDidStartTurnCountdown due to bad seconds")
            return nil
        }
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(playerIdentity)
        archiver.encode(seconds.uint8)
    }
}

/*-----------------------------------------*/

public struct PlayerDidEarnPointsAndCoins: ByteCoding {
    public static let type = MancalaGameServerToClientMessageType.playerDidEarnPointsAndCoins
    
    public let pointsEarned: Int
    public let coinsEarned: Int
    
    public init(pointsEarned: Int, coinsEarned: Int) {
        self.pointsEarned = pointsEarned
        self.coinsEarned = coinsEarned
    }
    
    public init?(message: Message) {
        guard message.type == PlayerDidEarnPointsAndCoins.type.rawValue, let payload = message.payload else {
            assert(false, "failed to unarchive PlayerDidEarnPointsAndCoins due to bad format")
            return nil
        }
        
        let unarchiver = ByteUnarchiver(archive: payload)
        self.init(unarchiver: unarchiver)
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        do {
            self.pointsEarned = Int(try unarchiver.decodeUInt16())
            self.coinsEarned = Int(try unarchiver.decodeUInt16())
        } catch {
            assert(false, "failed to unarchive PlayerDidEarnPointsAndCoins due to bad seconds")
            return nil
        }
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(pointsEarned.uint16)
        archiver.encode(coinsEarned.uint16)
    }
}

/*-----------------------------------------*/

public struct GameDidStartMessage: ByteCoding {
    public static let type = MancalaGameServerToClientMessageType.gameDidStart
    
    public let gameToken: GameToken
    public let startingPlayer: PlayerIdentity
    public let gameboardID: String
    public let startTime: Timestamp64
    
    public init(gameToken: GameToken, startingPlayer: PlayerIdentity, gameboardID: String, startTime: Timestamp64) {
        self.gameToken = gameToken
        self.startingPlayer = startingPlayer
        self.gameboardID = gameboardID
        self.startTime = startTime
    }
    
    public init?(message: Message) {
        guard message.type == GameDidStartMessage.type.rawValue, let payload = message.payload else {
            assert(false, "failed to unarchive GameDidStartMessage due to bad format")
            return nil
        }
        
        let unarchiver = ByteUnarchiver(archive: payload)
        self.init(unarchiver: unarchiver)
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        guard let gameToken = GameToken.init(unarchiver: unarchiver),
            let startingPlayer = PlayerIdentity(unarchiver: unarchiver) else { return nil }
        
        self.gameToken = gameToken
        self.startingPlayer = startingPlayer
        
        do {
            self.gameboardID = try unarchiver.decodeString()
            self.startTime = MancalaNetworkProtocolVersion.current >= .v1_1 ? try unarchiver.decodeUInt64() : 0
        } catch {
            assert(false, "failed to unarchive GameDidStartMessage due to bad gameboardID")
            return nil
        }
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(gameToken)
        archiver.encode(startingPlayer)
        archiver.encode(gameboardID)
        
        if MancalaNetworkProtocolVersion.current >= .v1_1 {
            archiver.encode(startTime)
        }
    }
}

/*-----------------------------------------*/

public extension Message {
    init(type: MancalaGameServerToClientMessageType, payload: ByteCoding? = nil) {
        let type = MessageType(type.rawValue)
        if let payload = payload {
            let archiver = ByteArchiver()
            payload.encode(with: archiver)
            self.init(type: type, payload: archiver.archive)
        } else {
            self.init(type: type, payload: nil)
        }
    }
    
    init(type: MancalaClientToGameServerMessageType, payload: ByteCoding? = nil) {
        let type = MessageType(type.rawValue)
        if let payload = payload {
            let archiver = ByteArchiver()
            payload.encode(with: archiver)
            self.init(type: type, payload: archiver.archive)
        } else {
            self.init(type: type, payload: nil)
        }
    }
}

public extension ByteArchiver {
    func encode(pitID: PitID) {
        assert(pitID < Int(UInt8.max), "invalid pitID for encoding")
        encode(pitID.uint8)
    }
}

public extension ByteUnarchiver {
    func decodePitID() throws -> PitID {
        return PitID(try decodeUInt8())
    }
}

extension Int {
    var uint8: UInt8 {
        #if swift(>=4.0)
            return UInt8(self)
        #else
            return UInt8(truncatingBitPattern: self)
        #endif
    }
    
    var uint16: UInt16 {
        #if swift(>=4.0)
            return UInt16(self)
        #else
            return UInt16(truncatingBitPattern: self)
        #endif
    }
}
