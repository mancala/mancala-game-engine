//
//  GameState.swift
//  Mancala
//
//  Created by Michael Sanford on 9/10/16.
//  Copyright © 2016 flipside5. All rights reserved.
//

import Foundation
import MancalaCore

public enum StartingMoves: CustomStringConvertible {
    case none
    case normal
    case four
    case fourOne
    
    public var is41: Bool {
        switch self {
        case .normal, .four, .none:
            return false
        case .fourOne:
            return true
        }
    }
    
    public var isFour: Bool {
        switch self {
        case .four:
            return true
        case .none, .normal, .fourOne:
            return false
        }
    }
    
    public var description: String {
        switch self {
        case .none:
            return "none"
        case .normal:
            return "normal"
        case .four:
            return "4"
        case .fourOne:
            return "4-1"
        }
    }
}

public enum GameOverReason {
    case completedGame
    case skippedTurn
    case determinedWinner
    
    public var isSkippedTurn: Bool {
        switch self {
        case .completedGame, .determinedWinner:
            return false
        case .skippedTurn:
            return true
        }
    }
}

public enum GameState: CustomDebugStringConvertible {
    case readyToPlay(currentPlayer: PlayerIdentity)
    case inProgress(currentPlayer: PlayerIdentity, winner: PlayerIdentity?, startingMoves: StartingMoves)
    case gameOver(winner: PlayerIdentity?, reason: GameOverReason, startingMoves: StartingMoves)
    case cancelled
    
    public var isDirty: Bool {
        switch self {
        case .readyToPlay: return false
        case .inProgress, .gameOver, .cancelled: return true
        }
    }
    
    public var isPlayable: Bool {
        switch self {
        case .readyToPlay, .inProgress: return true
        case .gameOver, .cancelled: return false
        }
    }
    
    public var currentPlayer: PlayerIdentity? {
        switch self {
        case .readyToPlay(let turn): return turn
        case .inProgress(let turn, _, _): return turn
        case .gameOver, .cancelled: return nil
        }
    }
    
    public var winner: PlayerIdentity? {
        switch self {
        case .inProgress(_, let winner, _): return winner
        case .gameOver(let winner, _, _): return winner
        default: return nil
        }
    }
    
    public var debugDescription: String {
        switch self {
        case .readyToPlay(let turn):
            return "Ready to Play (turn=\(turn))"
            
        case .inProgress(let turn, let winner, let startingMoves):
            if let winner = winner {
                return "In Progress (turn=\(turn), winner=\(winner), startingMoves=\(startingMoves))"
            }
            return "In Progress (turn=\(turn), startingMoves=\(startingMoves))"
            
        case .gameOver(let winner, let forced, let startingMoves):
            if let winner = winner {
                return "Game Over (winner=\(winner), forced=\(forced), startingMoves=\(startingMoves))"
            }
            return "Game Over (tie game)"
        case .cancelled:
            return "Game cancelled"
        }
    }
}

public func ==(lhs: GameState, rhs: GameState) -> Bool {
    switch (lhs, rhs) {
    case (.readyToPlay, .readyToPlay): return true
    case (.inProgress, .inProgress): return true
    case (.gameOver, .gameOver): return true
    case (.cancelled, .cancelled): return true
    default: return false
    }
}
