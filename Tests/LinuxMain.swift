import XCTest
@testable import MancalaGameEngineTests

XCTMain([
    testCase(MancalaGameEngineTests.allTests),
])
