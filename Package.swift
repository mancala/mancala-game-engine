// swift-tools-version:4.0

import PackageDescription

let package = Package(
    name: "MancalaGameEngine",
    products: [
        .library(
            name: "MancalaGameEngine",
            targets: ["MancalaGameEngine"]),
    ],
    dependencies: [
        .package(url: "git@gitlab.com:mancala/gaming-core.git", from: "1.0.1"),
        .package(url: "git@gitlab.com:mancala/mancala-core.git", from: "1.1.0")
    ],
    targets: [
        .target(
            name: "MancalaGameEngine",
            dependencies: ["MancalaCore", "GamingCore"]),
        .testTarget(
            name: "MancalaGameEngineTests",
            dependencies: ["MancalaGameEngine"]),
    ]
)
